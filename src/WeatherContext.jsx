import React, { useCallback, useReducer, useContext } from 'react'

const WeatherStateContext = React.createContext()
const WeatherDispatchContext = React.createContext()

const initialValue = {
    allWeather: {},
    allDataChart: {},
    allForecastItemList: {}
}

const WeatherContext = ({children}) => {
    const reducer = useCallback((state, action) => {
        switch (action.type){
            case 'SET_WEATHER':
                const weatherCity = action.payload
                const newAllWeather = {...state.allWeather, ...weatherCity}
                return {...state, allWeather: newAllWeather}
            case 'SET_CHART_DATA':
                const chartDataCity = action.payload
                const newChartData = {...state.allDataChart, ...chartDataCity}
                return {...state, allDataChart: newChartData}
            case 'SET_FORECAST_ITEM_LIST':
                const forecastItemList = action.payload
                const newForecastItemList = {...state.allForecastItemList, ...forecastItemList}
                return {...state, allForecastItemList: newForecastItemList}
            default:
                return state
        }
    },[])
    const [state, dispatch] = useReducer(reducer, initialValue)
    return (
        <WeatherDispatchContext.Provider value={dispatch}>
            <WeatherStateContext.Provider value={state}>
                {children}
            </WeatherStateContext.Provider>
        </WeatherDispatchContext.Provider>
    )
}

const useWeatherDispatchContext = () => {
    const dispatch = useContext(WeatherDispatchContext)
    if(!dispatch)
        throw Error("Must set dispatch provider")
    return dispatch
}
const useWeatherStateContext = () => {
    const state = useContext(WeatherStateContext)
    if(!state)
        throw Error("Must set state provider")
    return state
}

export default WeatherContext
export {
    useWeatherDispatchContext,
    useWeatherStateContext
}