
const citiesExample = [
    {city: "Jaén", country: "España", countryCode: "ES"},
    {city: "París", country: "Francia", countryCode: "FR"},
    {city: "Roma", country: "Italia", countryCode: "IT"},
]

export const getCities = () => citiesExample
export const getCountryNameByCountryCode = countryCode => citiesExample.filter( c => c.countryCode === countryCode)[0].country