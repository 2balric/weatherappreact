import React from 'react'
import PropTypes from 'prop-types'
import Alert from '@material-ui/lab/Alert'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'

import useCityList from './../../hooks/useCityList'
import { getCityCode } from './../../utils/utils'
import CityInfo from '../CityInfo'
import Weather from '../Weather'
import { useWeatherDispatchContext, useWeatherStateContext } from '../../WeatherContext'

const CityListItem = React.memo(({city, countryCode, country, weather, onClickCity}) => {
    return (
        <ListItem
        onClick={
            () => onClickCity(city, countryCode)
        } button>
            <Grid container 
                justify="center"
                alignItems="center"
            >
                <Grid item
                    xs={12}
                    md={9}
                >
                    <CityInfo city={city} country={country} />
                </Grid>
                <Grid item
                    xs={12}
                    md={3}
                >
                    <Weather temperature={weather?.temperature} state={weather && weather.state} /> 
                </Grid>
            </Grid>
        </ListItem>
    )
})

CityListItem.displayName = "CityListItem"

const renderCityAndCountry = onClickCity => (cityAndCountry, weather) => {
    const {city, countryCode} = cityAndCountry;
    const key = getCityCode(city, countryCode);
    return <CityListItem key={key} onClickCity={onClickCity} weather={weather} {...cityAndCountry}/>
}

const CityList = ({cities, onClickCity}) => {
    const actions = useWeatherDispatchContext()
    const data = useWeatherStateContext()
    const { allWeather } = data;
    const funcAux = renderCityAndCountry(onClickCity)
    const {error, setError} = useCityList(cities, allWeather, actions)
    return (
        <div>
            {
                error && <Alert severity="error" onClose={() => setError(null)}>{error}</Alert>
            }
            <List>
                {
                    cities.map(cityAndCountry => funcAux(cityAndCountry, allWeather[ getCityCode(cityAndCountry.city, cityAndCountry.countryCode)]))
                }
            </List>
        </div>
    )
}

CityList.propTypes = {
    cities: PropTypes.arrayOf(
        PropTypes.shape({
            city: PropTypes.string.isRequired,
            country: PropTypes.string.isRequired,
            countryCode: PropTypes.string.isRequired,
        })
    ).isRequired,
    onClickCity: PropTypes.func,
}

export default React.memo(CityList)
