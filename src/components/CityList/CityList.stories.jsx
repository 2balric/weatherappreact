import React from 'react'
import CityList from '.'
import { action } from '@storybook/addon-actions'

export default {
    title: "CityList",
    component: CityList
}

const citiesExample = [
    {city: "Jaén", country: "España"},
    {city: "París", country: "Francia"},
    {city: "Roma", country: "Italia"},
]
export const CityListExample = () => <CityList cities={citiesExample} onClickCity={action("Click an action")}/>