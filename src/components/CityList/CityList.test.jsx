import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import CityList from '.' //SUT: Subject under testing


const citiesExample = [
    {city: "Jaén", country: "España", countryCode: "ES"},
    {city: "París", country: "Francia", countryCode: "FR"},
    {city: "Roma", country: "Italia", countryCode: "IT"},
]

test("CityList renders", async () => {
    const { findAllByRole } = render(<CityList cities={citiesExample}/>)
    //Act
    const citiesComponents = await findAllByRole("button")
    
    //Assert
    expect(citiesComponents).toHaveLength(citiesExample.length);
})

test("CityList click on item", async () => {
    //Debemo simular una acción del usuario: click sobre un item
    // Para eso vamos a utilizar una funcion mock
    const fnClickOnItem = jest.fn()

    const { findAllByRole } = render(<CityList cities={citiesExample} onClickCity={fnClickOnItem}/>)
    const items = await findAllByRole("button")

    //FireEvent
    fireEvent.click(items[0]);

    expect(fnClickOnItem).toHaveBeenCalledTimes(1);

})