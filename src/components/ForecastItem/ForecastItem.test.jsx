import React from 'react'
import ForecastItem from '.'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

test("ForecastItem render", async () => {
    //AAA
    const { findByText } = render(<ForecastItem weekDay="Lunes" hour={15} state="clear" temperature={23}/>)
    const weekDay = await findByText("Lunes");
    expect(weekDay).toHaveTextContent("Lunes");
})