import React from 'react'
import 'typeface-roboto'
import ForecastItem from '.'

export default {
    title: "ForecastItem",
    component: ForecastItem
}

export const ForecastItemExample = () => <ForecastItem weekDay="Lunes" hour={15} state="clear" temperature={23}/>