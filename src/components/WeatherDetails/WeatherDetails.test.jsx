import React from 'react'
import WeatherDetails from '.'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

test("WeatherDetails render", async () => {
    //AAA
    const { findByText } = render(<WeatherDetails humidity={10} wind={23}/>)
    const wind = await findByText(/23/)
    const humidity = await findByText(/10/)

    expect(wind).toHaveTextContent("Viento: 23 km/h")
    expect(humidity).toHaveTextContent("Humedad: 10%")
})