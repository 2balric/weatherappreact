import React from 'react'
import 'typeface-roboto'
import WelcomeScreen from '.'

export default {
    title: "WelcomeScreen",
    component: WelcomeScreen
}

export const WelcomeScreenExample = () => <WelcomeScreen> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quia perspiciatis voluptatum praesentium cupiditate impedit, quibusdam adipisci culpa amet magni asperiores, corporis ullam nisi expedita eius quis in nostrum cumque? Veniam.</WelcomeScreen>