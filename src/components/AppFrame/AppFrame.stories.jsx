import React from 'react'
import AppFrame from '.'
import { BrowserRouter as Router } from 'react-router-dom'
export default {
    title: "AppFrame",
    component: AppFrame
}

export const AppFrameExample = () => (
    <Router>
        <AppFrame>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit deserunt temporibus adipisci eveniet. Maiores reprehenderit totam esse facere. Dolores minus inventore deleniti magni praesentium dolorum minima asperiores, nihil molestias debitis.
        </AppFrame>
    </Router>
)