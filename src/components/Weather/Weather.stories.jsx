import React from 'react'
import 'typeface-roboto'
import Weather from '.'

export default {
    title: "Weather",
    component: Weather
}

const WeatherTemplate = (args) => <Weather {...args}/>
export const WeatherCloud = WeatherTemplate.bind({})
WeatherCloud.args = {temperature: 10, state: "clouds"}
export const WeatherSunny = WeatherTemplate.bind({})
WeatherSunny.args = {temperature: 10, state: "clear"}

export const WeatherFog = WeatherTemplate.bind({})
WeatherFog.args = {temperature: 10, state: "drizzle"}

export const WeatherCloudy = WeatherTemplate.bind({})
WeatherCloudy.args = {temperature: 10, state: "snow"}

export const WeatherRain = WeatherTemplate.bind({})
WeatherRain.args = {temperature: 10, state: "rain"}