import React from 'react'
import 'typeface-roboto'
import ForecastChart from '.'

export default {
    title: "ForecastChart",
    component: ForecastChart
}

const data = [
    {
        "dayHour": "Vie 12", 
        "max": 25,
        "min": 18,
    },
    {
        "dayHour": "Sab 13", 
        "max": 20,
        "min": 15,
    },
    {
        "dayHour": "Dom 14", 
        "max": 21,
        "min": 14,
    },
    {
        "dayHour": "Lun 15", 
        "max": 20,
        "min": 18,
    },
    {
        "dayHour": "Mar 16", 
        "max": 25,
        "min": 18,
    },
    {
        "dayHour": "Mie 17", 
        "max": 27,
        "min": 23,
    },
    {
        "dayHour": "Jue 18", 
        "max": 29,
        "min": 25,
    }
]

export const ForecastChartExample = () => <ForecastChart data={data}/>