import React from 'react'
import 'typeface-roboto'
import Forecast from '.'

export default {
    title: "Forecast",
    component: Forecast
}
const forecastItemList = [
    { weekDay: "Lunes", hour: 15, state: "clear", temperature: 23 },
    { weekDay: "Martes", hour: 23, state: "clouds", temperature: 18 },
    { weekDay: "Miercoles", hour: 21, state: "drizzle", temperature: 15 },
    { weekDay: "Jueves", hour: 12, state: "rain", temperature: 1 }
]
export const ForecastExample = () => <Forecast forecastItemList={forecastItemList}/>