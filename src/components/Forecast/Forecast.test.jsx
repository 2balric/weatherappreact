import React from 'react'
import Forecast from '.'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

const forecastItemList = [
    { weekDay: "Lunes", hour: 15, state: "clear", temperature: 23 },
    { weekDay: "Martes", hour: 23, state: "clouds", temperature: 18 },
    { weekDay: "Miercoles", hour: 21, state: "drizzle", temperature: 15 },
    { weekDay: "Jueves", hour: 12, state: "rain", temperature: 1 }
]

test("Forecast render", async () => {
    //AAA
    const { findAllByTestId } = render(<Forecast forecastItemList={forecastItemList}/>)
    const allForecastItems = await findAllByTestId("forecast-item-container");
    expect(allForecastItems).toHaveLength(4);
})