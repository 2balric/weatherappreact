import React from 'react'
import 'typeface-roboto'
import CityInfo from '.'


export default {
    title: "CityInfo",
    component: CityInfo,
    argTypes: {
        city: { control: {type: "text"}},
        country: { control: {type: "text"}}
    }
}

export const CityExample = (args) => (<CityInfo {...args}></CityInfo>)
export const CityBuenosAires = () => <CityInfo city="Buenos Aires" country="Argentina"></CityInfo>

CityExample.args = {city:"Jaén", country:"España"}