import React, { useMemo } from 'react'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import LinearProgress from '@material-ui/core/LinearProgress'


import CityInfo from './../components/CityInfo'
import Weather from './../components/Weather'
import WeatherDetails from './../components/WeatherDetails'
import ForecastChart from './../components/ForecastChart'
import Forecast from './../components/Forecast'

import AppFrame from './../components/AppFrame'
import useCityPage from '../hooks/useCityPage'
import useCityList from '../hooks/useCityList'
import { getCityCode } from '../utils/utils'
import { getCountryNameByCountryCode } from './../services/serviceCities'
import { useWeatherDispatchContext, useWeatherStateContext } from './../WeatherContext'


const CityPage = () => {
    const actions = useWeatherDispatchContext()
    const data = useWeatherStateContext()
    //const { onSetAllWeather, onSetDataChart, onSetForecastItemList } = actions;
    const { allWeather, allDataChart, allForecastItemList } = data;
    const {city, countryCode} = useCityPage(allDataChart, allForecastItemList, actions)
    const cities = useMemo( () => ([{city, countryCode}]), [city, countryCode] ) //Se usa memo para no generar nuevas variables para los effects
    useCityList(cities, allWeather, actions)
    const weather = allWeather[getCityCode(city, countryCode)]
    const dataChart = allDataChart[getCityCode(city, countryCode)]
    const forecastItemList = allForecastItemList[getCityCode(city, countryCode)]

    const country = countryCode && getCountryNameByCountryCode(countryCode)
    const state = weather && weather.state
    const temperature = weather && weather.temperature
    const humidity = weather && weather.humidity
    const wind = weather && weather.wind
    return (
        <AppFrame>
            <Paper elevation={4}>
                <Grid container justify="space-around" direction="column" spacing={2}>
                    <Grid item container xs={12} justify="center" alignItems="flex-end">
                        <CityInfo city={city} country={country} />
                    </Grid>
                    <Grid container item xs={12} justify="center">
                        <Weather state={state} temperature={temperature} />
                        {
                            humidity && wind && <WeatherDetails humidity={humidity} wind={wind} />
                        }
                    </Grid>
                    <Grid item>
                        {
                            !dataChart && !forecastItemList && <LinearProgress />
                        }
                    </Grid>
                    <Grid item>
                        { 
                            dataChart && <ForecastChart dataChart={dataChart}/>
                        }
                    </Grid>
                    <Grid item>
                        {
                            forecastItemList && <Forecast forecastItemList={forecastItemList} />
                        }
                    </Grid>
                </Grid>
            </Paper>
        </AppFrame>
    )
}

export default CityPage
