import { useEffect, useDebugValue } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'

import { getForecastUrl } from './../utils/url'
import getChartData from '../utils/transform/getChartData'
import getForecastItemList from '../utils/transform/getForecastItemList'
import { getCityCode } from '../utils/utils'

const useCityPage = (allDataChart, allForecastItemList, actions) => {
    const {countryCode, city} = useParams()
    useDebugValue(`useCityPage ${city}`)
    useEffect( () => {
        const getForecast = async (cityCode) => {
            try{
                const { data, status } = await axios.get(getForecastUrl(city, countryCode));
                if(status === 200){
                    const dataAux = getChartData(data)
                    //onSetDataChart({ [cityCode]: dataAux})
                    actions({type: 'SET_CHART_DATA', payload: { [cityCode]: dataAux}})
                    const forecastItemListAux = getForecastItemList(data)
                    actions({type: 'SET_FORECAST_ITEM_LIST', payload: {[cityCode]: forecastItemListAux}})
                    //onSetForecastItemList({[cityCode]: forecastItemListAux})
                }
            }catch(error){
                console.log(error);
            }
        }
        const cityCode = getCityCode(city, countryCode);
        if(allDataChart && allForecastItemList && !allDataChart[cityCode] && !allForecastItemList[cityCode])
            getForecast(cityCode)
    }, [city, countryCode, actions, allDataChart, allForecastItemList])
    return {city, countryCode}
}

export default useCityPage