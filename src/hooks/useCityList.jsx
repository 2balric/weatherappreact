import { useState, useEffect} from 'react'
import axios from 'axios'

import { getWeatherUrl } from './../utils/url'
import getAllWeather from '../utils/transform/getAllWeather';
import { getCityCode } from '../utils/utils';

const useCityList = (cities, allWeather, actions) => {
//    const [allWeather, setAllWeather] = useState({}); //Hook del state
    const [error, setError] = useState(null);
    //Hook para el fetch
    useEffect( () => {
        const setWeather = async (city, country, countryCode) => {
            try{
                const propName = getCityCode(city, countryCode)
                actions({type: 'SET_WEATHER', payload: {[propName]: {} }})
                //onSetAllWeather({[propName]: {} })
                const response = await axios.get(getWeatherUrl(city, countryCode));
                const allWeatherAux = getAllWeather(response, city, countryCode)
                //setAllWeather( allWeather => ({ ...allWeather, ...allWeatherAux }))
                //onSetAllWeather(allWeatherAux)
                actions({type: 'SET_WEATHER', payload: allWeatherAux})
            }catch(error){
                if(error.response){ //errores del server
                    //const { data, status } = error.response;
                    setError("Error en el servidor. No podemos cargar la información. Pruebe en unos momentos.");
                }else if(error.request){ //No llegan al server
                    setError("Hubo un error de conexión. Compruebe su conexión a Internet.");
                }else{
                    setError("Error imprevisto");
                }
            }
        }
        cities.forEach(({city, country, countryCode}) => {
            if(!allWeather[getCityCode(city, countryCode)])
                setWeather(city, country, countryCode);
        });
    }, [cities, allWeather, actions]);

    return {error, setError}
}

export default useCityList