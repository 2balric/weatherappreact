import moment from 'moment'
import 'moment/locale/es'
import { toCelsius } from '../utils'

const getChartData = (data) => {
    const daysAhead = [0,1,2,3,4,5]
    const days = daysAhead.map( d => moment().add(d, 'd'));
    const dataAux = days.map( d => {
        const temperatureList = data.list.filter( el => {
            const dayOfYear = moment.unix(el.dt).dayOfYear()
            return dayOfYear === d.dayOfYear()
        }).map( el => el.main.temp)
        const max = toCelsius(Math.max(...temperatureList))
        const min = toCelsius(Math.min(...temperatureList))
        return {
            dayHour: d.format("ddd"),
            min: min,
            max: max,
        }
    })

    return dataAux
}
export default getChartData