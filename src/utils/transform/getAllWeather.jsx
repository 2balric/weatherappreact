import { validValues } from './../../components/IconState'
import { getCityCode, toCelsius } from './../utils'

const getAllWeather = (response, city, countryCode) => {
    const { data, status } = response;
    if(status !== 200)
    return null
    const temperature = toCelsius(data.main.temp);
    const humidity = data.main.humidity
    const wind = data.wind.speed
    const stateFromServer = data.weather[0].main.toLowerCase();
    const state = validValues.includes(stateFromServer) ? stateFromServer : null;
    const key = getCityCode(city, countryCode);
    const value = {temperature, state, humidity, wind};
    return ({ [key]: value })
}

export default getAllWeather