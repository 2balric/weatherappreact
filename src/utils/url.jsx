const appId = '80cbf5bfc20e210b197fb66140c87fc5'
const uriBase = 'http://api.openweathermap.org/data/2.5/'

export const getWeatherUrl = (city, countryCode) => `${uriBase}weather?q=${city},${countryCode}&appid=${appId}`
export const getForecastUrl = (city, countryCode) => `${uriBase}forecast?q=${city},${countryCode}&appid=${appId}`